**Transcriptional Bursting and Loop Extrusion (TBLE)**

The purpose of this repository is to confirm the **moderate** effect of loop domains (loop extrusion) on transcription level (transcriptional bursting) theoretically.

---

## Loop extrusion

To calculate the contact frequency in and out of a loop domain, the loop extrusion model was implemented and simulated.

"the median size of a cohesin-associated loop was 275kb"
Rao et al., 2017 http://doi.org/10.1016/j.cell.2017.09.026

I am implementing the 2 * 3 patterns.

(1-1) Loop extrusion condition
(1-2) No loop extrusion condition

(2-1) Non specific cohesin loading
(2-2) Specific cohesin loading at super-enhancer
(2-3) Specific cohesin laoding at multi enhancers

---

## Transcriptional bursting


---
