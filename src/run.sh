plugin=../externals/OpenMM/installed/lib/plugins
data_path=/work2/yuichi/Project_EP/TBLE/LE
LEFlags=(LE noLE)
SpFlags=(nonSp) #(sp-multi sp-SE nonSp)

for LEFlag in ${LEFlags[@]}; do
    for SpFlag in ${SpFlags[@]}; do
        hdf=${data_path}/${LEFlag}_${SpFlag}.hdf5
        LD_LIBRARY_PATH=../externals/OpenMM/installed/lib:../externals/OpenMM/installed/lib/plugins:../externals/HDF5/installed/lib:$LD_LIBRARY_PATH ./simulate_LE $hdf $plugin $LEFlag $SpFlags
    done
done


LEFlags=(LE noLE)
SpFlags=(sp-SE) #(sp-multi sp-SE nonSp)
birth_increases=(1 10 100)

for birth_increase in ${birth_increases[@]}; do
    for LEFlag in ${LEFlags[@]}; do
        for SpFlag in ${SpFlags[@]}; do
            hdf=${data_path}/${LEFlag}_${SpFlag}_${birth_increase}.hdf5
            LD_LIBRARY_PATH=../externals/OpenMM/installed/lib:../externals/OpenMM/installed/lib/plugins:../externals/HDF5/installed/lib:$LD_LIBRARY_PATH ./simulate_LE $hdf $plugin $LEFlag $SpFlags $birth_increase
        done
    done
done


<<EOF
n_samples=10000
n_bin=4
cmap=${data_path}/ref_reproduction_contact_map.txt
normed_cmap=${data_path}/ref_reproduction_contact_map_normed.txt
normed_bias=${data_path}/ref_reproduction_contact_map_bias.txt
bin_cmap=${data_path}/ref_reproduction_contact_map_binned.txt
bin_hmap=${data_path}/ref_reproduction_contact_map_binned.png
normed_bin_cmap=${data_path}/ref_reproduction_contact_map_binned_normed.txt
normed_bin_bias=${data_path}/ref_reproduction_contact_map_binned_bias.txt
normed_bin_hmap=${data_path}/ref_reproduction_contact_map_binned_normed.png
lef_gif=${data_path}/ref_reproduction_lef_dynamics.gif
start_step=9000 #1000 * 9
end_step=10000 #1000 * 9 + 1000 
start_pos=6300 #(300 + 600 + 1200) * 3
end_pos=10500 #(300 + 600 + 1200) * 5
boundaries="6300,6600,7200,8400,8700,9300,10500"




#LD_LIBRARY_PATH=../externals/OpenMM/installed/lib:../externals/OpenMM/installed/lib/plugins:../externals/HDF5/installed/lib:$LD_LIBRARY_PATH ./simulate_contact_map $hdf $n_samples $cmap
#./binning $cmap 4 $bin_cmap
#python draw_heatmap.py $bin_cmap $bin_hmap
#python make_movie_of_LEF_dynamics.py $hdf $start_step $end_step $start_pos $end_pos $boundaries $lef_gif
#python normalize.py $bin_cmap $normed_bin_cmap $normed_bin_bias
#python draw_heatmap.py $normed_bin_cmap $normed_bin_hmap
#python normalize.py $cmap $normed_cmap $normed_bias

EOF
